# TP_TDS_Antibes

TP de Traitement du Signal pour l'école d'Antibes Sport& IA

## Getting started

Installation des librairies nécessaires : On utilise le gestionnaire de librairie Miniconda

Télécharger la version selon votre système d'exploitation :
https://docs.conda.io/en/latest/miniconda.html

Dans un terminal de commande : Terminal (MacOSX, Linux), cmd (windows)

A partir du fichier yml vous créez votre environnement :

conda create --name myenv python=3.9

conda activate myenv

pip install numpy

pip install scipy

pip install matplotlib

pip install jupyterlab


## Lancement du TP

jupyter lab

ouvrir le fichier tp_tds_antibes.ipynb
